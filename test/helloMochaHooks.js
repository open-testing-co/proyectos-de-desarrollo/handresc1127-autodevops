const assert = require('assert');

describe('Framework Mocha pruebas de hooks', function () {

    before('Ejecución antes de todas las pruebas', function () {
        console.log('Ejecución antes de todas las pruebas');
    });

    after('Ejecución despues de todas las pruebas', function () {
        console.log('Ejecución despues de todas las pruebas');
    });

    beforeEach('Ejecución antes de cada una de las pruebas', function () {
        console.log('Ejecución antes de cada una de las pruebas');
    });

    beforeEach('Ejecución despues de cada una de las pruebas', function () {
        console.log('Ejecución despues de cada una de las pruebas');
    });

    it('Suma de dos numeros', function () {
        var a = 20;
        var b = 10;
        var c = a + b;
        assert.equal(c, 30);
    });

    it('Resta de dos numeros', function () {
        var a = 20;
        var b = 10;
        var c = a - b;
        assert.equal(c, 10);
    });

    it('Multiplicacion de dos numeros', function () {
        var a = 20;
        var b = 10;
        var c = a * b;
        assert.equal(c, 200);
    });

    it('Division de dos numeros', function () {
        var a = 20;
        var b = 10;
        var c = a / b;
        assert.equal(c, 2);
    });


})