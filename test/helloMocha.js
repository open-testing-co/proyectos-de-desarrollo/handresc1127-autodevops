//Add
//Sub
//Multiplication
//Division
const assert = require('assert');
describe('Framework Mocha pruebas basicas', function () {

    it('Suma de dos numeros', function () {
        var a = 20;
        var b = 10;
        var c = a + b;
        assert.equal(c, 30);
    });

    it('Resta de dos numeros', function () {
        var a = 20;
        var b = 10;
        var c = a - b;
        assert.equal(c, 10);
    });

    it('Multiplicacion de dos numeros', function () {
        var a = 20;
        var b = 10;
        var c = a * b;
        assert.equal(c, 200);
    });

    it('Division de dos numeros', function () {
        var a = 20;
        var b = 10;
        var c = a / b;
        assert.equal(c, 2);
    });

});