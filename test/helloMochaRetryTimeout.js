//Add
//Sub
//Multiplication
//Division
const assert = require('assert');
describe('Framework Mocha pruebas de Reintentos y Timeout', function () {
    var a = 20;
    var b = 10;
    //Timeout level TEST SUITE
    this.timeout(5000);
    it('Suma de dos numeros', function () {
        var c = a + b;
        assert.equal(c, 30);
    });

    it('Resta de dos numeros', function () {
        var c = a - b;
        assert.equal(c, 10);
    });

    it('Multiplicacion de dos numeros', function () {
        var c = a * b;
        assert.equal(c, 200);
    });

    it('Division de dos numeros', function () {
        this.timeout(250); //Timeout level TEST
        //Timeout level TEST
        //setTimeout(done, 300);
        var c = a / b;
        assert.equal(c, 2);
    });

});