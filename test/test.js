const request = require('supertest');
const app = require('../app');

describe('Pagina principal', function() {
  it('Bienvenida de la App', function(done) {
    request(app)
      .get('/')
      .expect(/Bienvenido a HANDRESC1127/, done);
  });
}); 
